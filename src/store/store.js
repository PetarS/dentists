import Vue from 'vue';
import Vuex from 'vuex';
import { companies } from './hardcoded-data';

Vue.use(Vuex);

const allFavourites = JSON.parse(localStorage.getItem("allFavorites"));

let state = {
    mapActive: true,
    allCompanies: companies,
    locationId: 1,
    companyId: 1,
    favoritesIdArray: allFavourites==null ? [] : allFavourites
};

const getters = {
    companies: state => {
        return state.allCompanies.filter(function (el) {
            return el.locationId == state.locationId;
        })
    },
    locationId: state => {
        return state.locationId;
    },
    company: state => {
        return state.allCompanies.filter(function (el) {
            return el.id == state.companyId;
        })
    },
    favorites: state => {
        let favorites = state.allCompanies.filter(company => state.favoritesIdArray.includes(company.id));
        return favorites;
    }
};

const mutations = {
    switchMap: state => {
        state.mapActive = !state.mapActive;
    },
    setLocationId: (state, payload) => {
        state.locationId = payload;
    },
    setCompanyId: (state, payload) => {
        state.companyId = payload;
    },
    toggleHoveredCompany: (state, payload) => {
        state.allCompanies[payload].hovered = !state.allCompanies[payload].hovered;
    }
}

const actions = {
    switchMap: ({ commit }) => {
        commit('switchMap');
    },
    setLocationId: ({ commit }, payload) => {
        commit('setLocationId', payload);
    },
    setCompanyId: ({ commit }, payload) => {
        commit('setCompanyId', payload);
    },
    toggleHoveredCompany: ({ commit , state }, payload) => {
        for (let i = 0; i < state.allCompanies.length; i++) {
            if(state.allCompanies[i].id==payload) {
                commit('toggleHoveredCompany', i);
            }
        }
    }
}

export const store = new Vuex.Store({
    state,
    mutations,
    getters,
    actions,
});
