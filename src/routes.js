import Home from './components/pages/Home.vue';
import Locations from './components/pages/Locations.vue';
import CompanyProfile from './components/pages/CompanyProfile.vue';
import Favorites from './components/Favorites.vue';

export const routes = [
    { path: '', component: Home },
    { path: '/locations/:cityId', component: Locations },
    { path: '/location/:locationId', component: CompanyProfile },
    { path: '/favorites', component: Favorites }
]
