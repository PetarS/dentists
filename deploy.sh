#!/usr/bin/env bash

# VARIABLES
username=kraljina
host=happyfist.com
project=/var/www/html/dentists.happyfist.com
now=`date +%Y-%m-%d.%H_%M_%S`

npm run build
rsync -az --delete --progress --include="./dest" --include="index.html" $username@$host:$project